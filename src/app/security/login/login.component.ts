import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  bourreCochon: FormGroup;
  processRunning = false;
  private formSubmitAttempt: boolean;

  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    public router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.bourreCochon = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  isFieldInvalid(field: string) {
    return (
      (this.bourreCochon.get(field).valid &&
        this.bourreCochon.get(field).touched) ||
      (this.bourreCochon.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    if (this.bourreCochon.valid) {
      this.processRunning = true;
      this.authenticationService
        .loginWithRole(
          this.bourreCochon.get('userName').value,
          this.bourreCochon.get('password').value,
          'ROLE_SUPER_ADMIN'
        )
        .subscribe(
          () => {
            if (AuthenticationService.isLoggedIn()) {
              this.processRunning = false;
              this.router.navigate([this.authenticationService.redirectUrl]);
            } else {
              throw new Error();
            }
          },
          (error: HttpErrorResponse) => {
            this.processRunning = false;
            this.snackBar.open('Login ou mot de passe invalide !', 'X');
          }
        );
    }
    this.formSubmitAttempt = true;
  }
}
