import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { UserLdap } from '../models/user-ldap';
import { LDAP_USERS } from '../models/users-ldap-data';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  users: UserLdap[] = LDAP_USERS;

  private usersUrl = 'api/users';
  private httpOptions = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) {}

  addUser(user: UserLdap): Observable<UserLdap> {
    return this.http.post<UserLdap>(this.usersUrl, user, {
      headers: this.httpOptions,
    });
  }

  updateUser(userToUpdate: UserLdap): Observable<UserLdap> {
    return this.http.put<UserLdap>(
      this.usersUrl + '/' + userToUpdate.id,
      userToUpdate,
      { headers: this.httpOptions }
    );
  }

  getUser(id: number): Observable<UserLdap> {
    return this.http.get<UserLdap>(this.usersUrl + '/' + id);
  }

  getUsers(): Observable<UserLdap[]> {
    return this.http.get<UserLdap[]>(this.usersUrl);
  }

  deleteUser(id: number): Observable<UserLdap> {
    return this.http.delete<UserLdap>(this.usersUrl + '/' + id, {
      headers: this.httpOptions,
    });
  }
}
