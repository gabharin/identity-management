import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LdapManagementRoutingModule } from './ldap-management-routing.module';
import { LdapAddComponent } from './ldap-add/ldap-add.component';
import { LdapEditComponent } from './ldap-edit/ldap-edit.component';
import { LdapListComponent } from './ldap-list/ldap-list.component';
import { AlertComponent } from '../share/alert/alert.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppMaterialModule } from '../app-material.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryUsersService } from '../services/in-memory-users.service';
import { LdapComponent } from './ldap-component/ldap.component';
import { NavbarComponent } from '../navbar/navbar.component';

@NgModule({
  declarations: [
    LdapAddComponent,
    LdapEditComponent,
    LdapListComponent,
    AlertComponent,
    LdapComponent,
    NavbarComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    LdapManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AppMaterialModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryUsersService, {
      dataEncapsulation: false,
    }),
  ],
})
export class LdapManagementModule {}
