import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Role, UserLdap } from '../../models/user-ldap';
import { FormBuilder } from '@angular/forms';
import { OnInit } from '@angular/core';
import {
  ConfirmValidParentMatcher,
  passwordValidator,
} from './passwords-validator.directive';
import { InMemoryUsersService } from 'src/app/services/in-memory-users.service';

export abstract class LdapDetailComponent {
  user: UserLdap;
  userForm = this.fb.group({
    login: [''],
    nom: [''],
    prenom: [''],
    passwordGroup: this.fb.group(
      {
        password: [''],
        passwordConfirm: [''],
      },
      { validators: passwordValidator }
    ),
    mail: { value: '', disabled: true },
    employeNumero: { value: '', disabled: !this.addForm },
    employeNiveau: { value: '', disabled: !this.addForm },
    dateEmbauche: { value: '', disabled: !this.addForm },
    publisherId: { value: '', disabled: !this.addForm },
    active: [true],
  });
  passwordPlaceholder: string;

  processLoadRunning = false;
  processValidateRunning = false;
  errorMessage = '';
  confirmValidParentMatcher = new ConfirmValidParentMatcher();

  protected constructor(
    public addForm: boolean,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.passwordPlaceholder =
      'Mot de passe' + (this.addForm ? '' : ' (vide si inchangé)');
  }

  protected onInit() {}
  private formGetValue(name: string): any {
    return this.userForm.get(name).value;
  }
  goToLdap(): void {
    this.router.navigate(['/users/list']);
  }

  isFormValid(): boolean {
    return (
      this.userForm.valid &&
      (!this.addForm || this.formGetValue('passwordGroup.password') !== '')
    );
  }
  abstract validateForm(): void;
  onSubmitForm(): void {
    this.validateForm();
  }

  updateLogin(): void {
    if (this.addForm) {
      this.userForm
        .get('login')
        .setValue(
          (
            this.formGetValue('prenom') +
            '.' +
            this.formGetValue('nom')
          ).toLowerCase()
        );
      this.updateMail();
    }
  }
  updateMail(): void {
    if (this.addForm) {
      this.userForm
        .get('mail')
        .setValue(this.formGetValue('login').toLowerCase() + '@epsi.lan');
    }
  }

  protected copyUserToFormControl(): void {
    const controlsToUse = [
      'login',
      'nom',
      'prenom',
      'mail',
      'employeNumero',
      'employeNiveau',
      'dateEmbauche',
      'publisherId',
      'active',
    ];
    Object.keys(this.userForm.controls).forEach((control) => {
      if (controlsToUse.includes(control)) {
        this.userForm.get(control).setValue(this.user[control]);
      }
    });
  }

  protected getUserFromFormControl(): UserLdap {
    return {
      id: 0,
      login: this.userForm.get('login').value,
      nom: this.userForm.get('nom').value,
      prenom: this.userForm.get('prenom').value,
      nomComplet:
        this.userForm.get('nom').value +
        ' ' +
        this.userForm.get('prenom').value,
      mail: this.userForm.get('mail').value,
      employeNumero: parseInt(this.userForm.get('employeNumero').value),
      employeNiveau: parseInt(this.userForm.get('employeNiveau').value),
      dateEmbauche: this.userForm.get('dateEmbauche').value,
      publisherId: parseInt(this.userForm.get('publisherId').value),
      active: this.userForm.get('active').value,
      motDePasse: this.userForm.get('passwordGroup.password').value,
      role: Role.ROLE_USER,
    };
  }
}
