import { FormControl, FormGroup, FormGroupDirective, NgForm, ValidationErrors, ValidatorFn } from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";

export const passwordValidator: ValidatorFn = (form: FormGroup): ValidationErrors | null => {
    const password = form.get('password');
    const passwordConfirm = form.get('passwordConfirm');

    return password 
    && passwordConfirm 
    && password.value === passwordConfirm.value ? null : { passwordValidator : true };
}

export class ConfirmValidParentMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm): boolean {
        return control.parent.invalid && control.touched;
    }
}